/**
* University of Naples Federico II
* Master of Embedded System
*
* Academic Year: 2015-2016
*
* Group Number: 2
* Engineers: Castello Oscar
*            Iorio Raffaele
*/

/**
 * Questo progetto implementa la funzionalità di buzzer per permettere alla board di
 * suonare semplici melodie monofoniche. Per farlo, sfrutta i timer TIM presenti sul
 * SOC STM32F407VG ed in particolare la funzione PWM di tali timer. Infatti, modulando
 * opportunamente in frequenza un onda PWM, è possibile pilotare un piccolo piezo-buzzer
 * o un piccolo artoparlante da cellulare, per riprodurre diversi toni musicali che,
 * opportunamente riprodotti in sequenza e secondo precisi tempi, permettono di generare
 * melodie.
 * Si implementa in oltre una versione molto piu precisa della funzione di Delay, che
 * sfrutta l'interrupt hardware SysTick per generare pause di millisecondi in maniera più
 * accurata.
 *
 * Il tutto viene generato sul timer TIM4 collegato allo stesso GPIO dei LED e riprodotto
 * sul pin collegato al led GREEN, per poter osservare (oltre che ascoltare) la modulazione.
 *
 * E' necessario il seguente hardware aggiuntivo per poter udire i toni generati. Sono integrate
 * le melodie tratte dai giochi storici The Secret of Monkey Island e Tetris Theme Long Version.
 *
 * VDD -------------
 *                  |+
 *            BUZZ | | )))
 *                  |
 *                |/
 * PB12 -\/\/\----|
 *      500 Ohm   |\
 *                  |
 *                  |
 * GND -------------
 */

#include "stm32f4xx.h"
#include "notes.h"

static TIM_HandleTypeDef BUZZER_TIM_HandleStruct;
static TIM_OC_InitTypeDef BUZZER_TIM_OCStruct;

/**
 * @brief	Initialize the GPIO PB1 and TIMER TIM3 for buzzer
 * @param	None
 * @retval	None
 */
void Melody_Init(){
	GPIO_InitTypeDef GPIO_InitStruct;

	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_TIM3_CLK_ENABLE();

	GPIO_InitStruct.Pin = GPIO_PIN_1;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF2_TIM3;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	BUZZER_TIM_HandleStruct.Instance = TIM3;   //Same timer whose clocks we enabled
	BUZZER_TIM_HandleStruct.Init.Prescaler = HAL_RCC_GetHCLKFreq() / 4000000;
	BUZZER_TIM_HandleStruct.Init.CounterMode = TIM_COUNTERMODE_UP;
	BUZZER_TIM_HandleStruct.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;

	BUZZER_TIM_HandleStruct.Init.Period = 40000;
	BUZZER_TIM_HandleStruct.Channel = HAL_TIM_ACTIVE_CHANNEL_4;
	HAL_TIM_OC_Init(&BUZZER_TIM_HandleStruct);

	BUZZER_TIM_OCStruct.OCMode = TIM_OCMODE_PWM1;
	BUZZER_TIM_OCStruct.OCIdleState = TIM_OCIDLESTATE_SET;
	BUZZER_TIM_OCStruct.OCPolarity = TIM_OCPOLARITY_HIGH;
	BUZZER_TIM_OCStruct.OCFastMode = TIM_OCFAST_ENABLE;

	BUZZER_TIM_OCStruct.Pulse = 20000;
	HAL_TIM_OC_ConfigChannel(&BUZZER_TIM_HandleStruct, &BUZZER_TIM_OCStruct, TIM_CHANNEL_4);
}

/**
 * @brief	This function change the frequency of the TIM and set the duty cycle of the PWM
 * 			to 50% trough the selected period.
 * @param	freq is the frequency of the note to play
 * @param	duration is the duration of the note to play on and off
 * @retval	None
 */

/**
 * freqHz=(SystemCoreClock/Prescaler)/Period
 * freqHz*Period*Prescaler = SystemCoreClock
 * Period=SystemCoreClock/freqHz*Prescaler
 */
void BUZZER_Frequency(uint16_t freq, uint16_t duration)
{

	// Calculate the period
	uint16_t b_period = HAL_RCC_GetHCLKFreq() / (freq * BUZZER_TIM_HandleStruct.Init.Prescaler);
	// Set the period of the timer
	BUZZER_TIM_HandleStruct.Init.Period = b_period;
	BUZZER_TIM_HandleStruct.Channel = HAL_TIM_ACTIVE_CHANNEL_4;
	HAL_TIM_OC_Init(&BUZZER_TIM_HandleStruct);
	// Set the duty cycle
	BUZZER_TIM_OCStruct.Pulse = b_period >> 1;;
	//HAL_TIM_OC_Init(&BUZZER_TIM_HandleStruct);
	HAL_TIM_OC_ConfigChannel(&BUZZER_TIM_HandleStruct, &BUZZER_TIM_OCStruct, TIM_CHANNEL_4);

	/* Start and Stop count on TIM4 for duration */
	//HAL_TIM_Base_Start(&BUZZER_TIM_HandleStruct);
	HAL_TIM_OC_Start(&BUZZER_TIM_HandleStruct, TIM_CHANNEL_4);

    HAL_Delay(duration);

	//HAL_TIM_Base_Stop(&BUZZER_TIM_HandleStruct);
	HAL_TIM_OC_Stop(&BUZZER_TIM_HandleStruct, TIM_CHANNEL_4);
}
